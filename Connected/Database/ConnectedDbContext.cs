﻿using Connected.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database
{
    public class ConnectedDbContext : DbContext
    {
        public DbSet<Disease> Diseases { get; set; }
        public DbSet<Person> Persons { get; set; }
        //public DbSet<PersonDisease> PersonDiseases { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<DoctorPacientDisease> DoctorPacientDiseases { get; set; }
        public ConnectedDbContext(DbContextOptions<ConnectedDbContext> options) : base(options) { }


        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder =>
      {
          builder
              .AddFilter((category, level) =>
                  category == DbLoggerCategory.Database.Command.Name
                  && level == LogLevel.Information)
              .AddDebug();
      });
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(MyLoggerFactory)  //tie-up DbContext with LoggerFactory object
                .EnableSensitiveDataLogging()
                .UseSqlServer(@"Server=localhost\MSSQLSERVER01;Database=connected;Trusted_Connection=True;ConnectRetryCount=0;MultipleActiveResultSets=true");
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Entity<PersonDisease>()
            //    .ToTable("PersonDisease")
            //    .HasKey(t => new { t.PersonId, t.DiseaseId });

            //builder.Entity<PersonDisease>()
            //    .HasOne(pt => pt.Disease)
            //    .WithMany(p => p.PersonDiseases)
            //    .HasForeignKey(pt => pt.DiseaseId);

            //builder.Entity<PersonDisease>()
            //    .HasOne(pt => pt.Person)
            //    .WithMany(t => t.PersonDiseases)
            //    .HasForeignKey(pt => pt.PersonId);


            builder.Entity<DoctorPacientDisease>()
                .ToTable("DoctorPatientDisease")
                .HasKey(t => new { t.PersonId, t.DoctorId, t.DiseaseId });

            builder.Entity<DoctorPacientDisease>()
                .HasOne(pt => pt.Doctor)
                .WithMany(p => p.DoctorPacientsDisease)
                .HasForeignKey(pt => pt.DoctorId);

            builder.Entity<DoctorPacientDisease>()
                .HasOne(pt => pt.Person)
                .WithMany(t => t.DoctorPacientsDisease)
                .HasForeignKey(pt => pt.PersonId);

            builder.Entity<DoctorPacientDisease>()
                .HasOne(pt => pt.Disease)
                .WithMany(t => t.DoctorPacientDiseases)
                .HasForeignKey(pt => pt.DiseaseId);
        }
    }
}
