﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database
{
    public static class DataSeeder
    {
        public static void Seed(this ConnectedDbContext dbContext)
        {
            if (!dbContext.Persons.Any())
            {
                dbContext.Persons.Add(
                    new Models.Person()
                    {
                        Name = "John",
                        DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                    });

                dbContext.Persons.Add(
                    new Models.Person()
                    {
                        Name = "Michael",
                        DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                    });

                dbContext.Diseases.Add(
                    new Models.Disease()
                    {
                        Name = "Flue",
                        DoctorPacientDiseases = new List<Models.DoctorPacientDisease>()
                    });

                dbContext.Diseases.Add(
                        new Models.Disease()
                        {
                            Name = "Asthma",
                            DoctorPacientDiseases = new List<Models.DoctorPacientDisease>()
                        });

                dbContext.Doctors.Add(
                    new Models.Doctor()
                    {
                        Name = "DOctor M",
                        DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                    });

                dbContext.Doctors.Add(
                        new Models.Doctor()
                        {
                            Name = "Doctor X",
                            DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                        });


                dbContext.SaveChanges();

                dbContext.Persons.FirstOrDefault().DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                {
                    new Models.DoctorPacientDisease()
                    {
                        Disease = dbContext.Diseases.FirstOrDefault(),
                        Doctor = dbContext.Doctors.FirstOrDefault()
                    }
                };

                dbContext.Persons.Skip(1).FirstOrDefault().DoctorPacientsDisease = new List<Models.DoctorPacientDisease>()
                {
                    new Models.DoctorPacientDisease()
                    {
                        Disease = dbContext.Diseases.Skip(1).FirstOrDefault(),
                        Doctor = dbContext.Doctors.Skip(1).FirstOrDefault()
                    }
                };

                dbContext.SaveChanges();
            }
        }
    }
}
