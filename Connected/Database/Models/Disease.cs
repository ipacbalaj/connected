﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Models
{
    public class Disease
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public List<PersonDisease> PersonDiseases { get; set; }
        public List<DoctorPacientDisease> DoctorPacientDiseases { get; set; }
    }
}
