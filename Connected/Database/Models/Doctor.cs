﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Models
{
    public class Doctor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DoctorPacientDisease> DoctorPacientsDisease { get; set; }
    }
}
