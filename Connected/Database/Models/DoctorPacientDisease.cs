﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Models
{
    public class DoctorPacientDisease
    {
        public int PersonId { get; set; }
        public int? DoctorId { get; set; }
        public int DiseaseId { get; set; }
        public Person Person { get; set; }
        public Doctor Doctor { get; set; }
        public Disease Disease { get; set; }
    }
}
