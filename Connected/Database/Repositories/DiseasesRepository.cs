﻿using Connected.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Repositories
{
    public class DiseasesRepository
    {
        private readonly ConnectedDbContext _dbContext;

        public DiseasesRepository(ConnectedDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IQueryable<Disease> GetAll()
        {
            return _dbContext.Diseases;
        }
    }
}
