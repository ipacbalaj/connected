﻿using Connected.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Repositories
{
    public class DoctorPatientDiseaseRepository
    {
        private readonly ConnectedDbContext _dbContext;

        public DoctorPatientDiseaseRepository(ConnectedDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IQueryable<DoctorPacientDisease> GetAll()
        {
            return _dbContext.DoctorPacientDiseases;
        }
    }
}
