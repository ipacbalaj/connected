﻿using Connected.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Repositories
{
    public class DoctorRepository
    {
        private readonly ConnectedDbContext _dbContext;

        public DoctorRepository(ConnectedDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IQueryable<Doctor> GetAll()
        {
            return _dbContext.Doctors;
        }
    }
}
