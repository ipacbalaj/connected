﻿using Connected.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Database.Repositories
{
    public class PersonsRepository
    {
        private readonly ConnectedDbContext _dbContext;

        public PersonsRepository(ConnectedDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IEnumerable<Person> GetAll()
        {
            return _dbContext.Persons;
        }

        public async Task<Person> GetById(int id)
        {
            return await _dbContext.Persons.FirstOrDefaultAsync(item => item.Id == id);
        }
    }
}
