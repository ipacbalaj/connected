﻿using Connected.Graphql.Queries;
using GraphQL;
using GraphQL.Types;

namespace Connected.Graphql
{
    public class ConnectedSchema : Schema
    {
        // schema uses an abstraction of whatever di you are using
        public ConnectedSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<ConnectedQuery>();            
        }
    }
}
