﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Queries
{
    public class ConnectedQuery : ObjectGraphType
    {
        public ConnectedQuery()
        {
            Name = "ConnectedQuery";
            Field<PersonQuery>("persons", resolve: context => new { });
            Field<DoctorQuery>("doctors", resolve: context => new { });
            Field<DiseasesQuery>("diseases", resolve: context => new { });
        }
    }
}
