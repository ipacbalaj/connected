﻿using Connected.Database.Repositories;
using Connected.Graphql.Types;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Queries
{
    public class DiseasesQuery : ObjectGraphType
    {
        public DiseasesQuery(DiseasesRepository diseasesRepository)
        {
            //graphql .net takes care of the conversion from Person to PersonType
            Field<ListGraphType<DiseaseType>>("all", resolve: context => diseasesRepository.GetAll());
        }
    }
}
