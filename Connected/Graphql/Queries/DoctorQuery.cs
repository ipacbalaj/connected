﻿using Connected.Database.Repositories;
using Connected.Graphql.Types;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Queries
{
    public class DoctorQuery : ObjectGraphType
    {
        public DoctorQuery(DoctorRepository doctorRepository)
        {
            //graphql .net takes care of the conversion from Person to PersonType
            Field<ListGraphType<DoctorType>>("all", resolve: context => doctorRepository.GetAll());
        }
    }
}
