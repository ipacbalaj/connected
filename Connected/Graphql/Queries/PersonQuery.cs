﻿using Connected.Database.Repositories;
using Connected.Graphql.Types;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Queries
{
    public class PersonQuery : ObjectGraphType
    {
        public PersonQuery(PersonsRepository personsRepository)
        {
            //graphql .net takes care of the conversion from Person to PersonType
            Field<ListGraphType<PersonType>>("allPersons", resolve: context => personsRepository.GetAll());
            Field<PersonType>("person",
                                "Retrieve Person by Id",
                                arguments: new QueryArguments()
                                {
                                     new QueryArgument<IntGraphType> { Name = "id"}
                                },
                                resolve: context =>
                                {
                                    var id = context.GetArgument<int>("id");
                                    return personsRepository.GetById(id);
                                });
        }
    }
}
