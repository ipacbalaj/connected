﻿using Connected.Database.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Types
{
    public class DiseaseType : ObjectGraphType<Disease>
    {
        public DiseaseType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
        }
    }
}
