﻿using Connected.Database.Models;
using Connected.Services;
using GraphQL.DataLoader;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Types
{
    public class DoctorType : ObjectGraphType<Doctor>
    {
        public DoctorType(DiseasesService diseasesService, IDataLoaderContextAccessor dataLoaderContextAccessor)
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field<ListGraphType<PersonType>>("patients", resolve: context =>
            {
                var loader = dataLoaderContextAccessor.Context.GetOrAddCollectionBatchLoader<int, Person>(
                    "GetPatientsForDoctor", diseasesService.GetPatientsForDoctorLookUp);
                return loader.LoadAsync(context.Source.Id);
            });
        }
    }
}
