﻿using Connected.Database.Models;
using Connected.Database.Repositories;
using Connected.Services;
using GraphQL.DataLoader;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Graphql.Types
{
    // the schema exposes methadata, so by specifiang Person as metadata here you provide that to the schema
    public class PersonType : ObjectGraphType<Person>
    {
        public PersonType(DiseasesService diseasesService, IDataLoaderContextAccessor dataLoaderContextAccessor)
        {
            // the actual graph type is infered IntGraphTYpe/StringGraphType
            Field(x => x.Id);
            Field(x => x.Name);
            Field<ListGraphType<DiseaseType>>("diseases",
                resolve: context => diseasesService.GetDiseasesForPerson(context.Source.Id));
            Field<ListGraphType<DiseaseType>>("diseasesUsingLoader", resolve: context =>
            {

                var loader = dataLoaderContextAccessor.Context.GetOrAddCollectionBatchLoader<int, Disease>(
                    "GetDisesesByPersonId", diseasesService.GetDiseasesForPersonLookUp);
                return loader.LoadAsync(context.Source.Id);
            });
        }
    }
}
