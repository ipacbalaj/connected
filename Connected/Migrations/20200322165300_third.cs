﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Connected.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonDisease_Persons_DiseaseId",
                table: "PersonDisease");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonDisease_Diseases_PersonId",
                table: "PersonDisease");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonDisease_Diseases_DiseaseId",
                table: "PersonDisease",
                column: "DiseaseId",
                principalTable: "Diseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonDisease_Persons_PersonId",
                table: "PersonDisease",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonDisease_Diseases_DiseaseId",
                table: "PersonDisease");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonDisease_Persons_PersonId",
                table: "PersonDisease");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonDisease_Persons_DiseaseId",
                table: "PersonDisease",
                column: "DiseaseId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonDisease_Diseases_PersonId",
                table: "PersonDisease",
                column: "PersonId",
                principalTable: "Diseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
