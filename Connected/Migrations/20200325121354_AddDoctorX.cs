﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Connected.Migrations
{
    public partial class AddDoctorX : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatients_Diseases_DiseaseId",
                table: "DoctorPatients");

            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatients_Doctors_DoctorId",
                table: "DoctorPatients");

            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatients_Persons_PersonId",
                table: "DoctorPatients");

            migrationBuilder.DropTable(
                name: "PersonDisease");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DoctorPatients",
                table: "DoctorPatients");

            migrationBuilder.RenameTable(
                name: "DoctorPatients",
                newName: "DoctorPatientDisease");

            migrationBuilder.RenameIndex(
                name: "IX_DoctorPatients_DoctorId",
                table: "DoctorPatientDisease",
                newName: "IX_DoctorPatientDisease_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_DoctorPatients_DiseaseId",
                table: "DoctorPatientDisease",
                newName: "IX_DoctorPatientDisease_DiseaseId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DoctorPatientDisease",
                table: "DoctorPatientDisease",
                columns: new[] { "PersonId", "DoctorId", "DiseaseId" });

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatientDisease_Diseases_DiseaseId",
                table: "DoctorPatientDisease",
                column: "DiseaseId",
                principalTable: "Diseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatientDisease_Doctors_DoctorId",
                table: "DoctorPatientDisease",
                column: "DoctorId",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatientDisease_Persons_PersonId",
                table: "DoctorPatientDisease",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatientDisease_Diseases_DiseaseId",
                table: "DoctorPatientDisease");

            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatientDisease_Doctors_DoctorId",
                table: "DoctorPatientDisease");

            migrationBuilder.DropForeignKey(
                name: "FK_DoctorPatientDisease_Persons_PersonId",
                table: "DoctorPatientDisease");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DoctorPatientDisease",
                table: "DoctorPatientDisease");

            migrationBuilder.RenameTable(
                name: "DoctorPatientDisease",
                newName: "DoctorPatients");

            migrationBuilder.RenameIndex(
                name: "IX_DoctorPatientDisease_DoctorId",
                table: "DoctorPatients",
                newName: "IX_DoctorPatients_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_DoctorPatientDisease_DiseaseId",
                table: "DoctorPatients",
                newName: "IX_DoctorPatients_DiseaseId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DoctorPatients",
                table: "DoctorPatients",
                columns: new[] { "PersonId", "DoctorId", "DiseaseId" });

            migrationBuilder.CreateTable(
                name: "PersonDisease",
                columns: table => new
                {
                    PersonId = table.Column<int>(type: "int", nullable: false),
                    DiseaseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonDisease", x => new { x.PersonId, x.DiseaseId });
                    table.ForeignKey(
                        name: "FK_PersonDisease_Diseases_DiseaseId",
                        column: x => x.DiseaseId,
                        principalTable: "Diseases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonDisease_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PersonDisease_DiseaseId",
                table: "PersonDisease",
                column: "DiseaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatients_Diseases_DiseaseId",
                table: "DoctorPatients",
                column: "DiseaseId",
                principalTable: "Diseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatients_Doctors_DoctorId",
                table: "DoctorPatients",
                column: "DoctorId",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorPatients_Persons_PersonId",
                table: "DoctorPatients",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
