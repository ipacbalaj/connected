﻿using Connected.Database.Models;
using Connected.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connected.Services
{
    public class DiseasesService
    {
        private readonly DiseasesRepository _diseasesRepository;
        private readonly PersonsRepository _personsRepository;
        private readonly DoctorRepository _doctorRepository;
        private readonly DoctorPatientDiseaseRepository _doctorPatientDiseaseRepository;

        public DiseasesService(DiseasesRepository diseasesRepository,
            PersonsRepository personsRepository,
            DoctorRepository doctorRepository,
            DoctorPatientDiseaseRepository doctorPatientDiseaseRepository)
        {
            this._diseasesRepository = diseasesRepository;
            this._personsRepository = personsRepository;
            this._doctorRepository = doctorRepository;
            this._doctorPatientDiseaseRepository = doctorPatientDiseaseRepository;
        }

        public IEnumerable<Disease> GetDiseasesForPerson(int personId)
        {
            var result = from d in _diseasesRepository.GetAll()
                         join pd in _doctorPatientDiseaseRepository.GetAll()
                            on d.Id equals pd.DiseaseId
                         where pd.PersonId == personId
                         select d;

            return result;
        }

        public async Task<ILookup<int, Disease>> GetDiseasesForPersonLookUp(IEnumerable<int> personIds)
        {
            // todo: make this async
            var result = from d in _diseasesRepository.GetAll()
                         join pd in _doctorPatientDiseaseRepository.GetAll()
                            on d.Id equals pd.DiseaseId
                         where personIds.Contains(pd.PersonId)
                         select new
                         {
                             PersonId = pd.PersonId,
                             Disease = d
                         };

            return result.ToLookup(item => item.PersonId, item => item.Disease);
        }

        public async Task<ILookup<int, Person>> GetPatientsForDoctorLookUp(IEnumerable<int> doctorIds)
        {
            // todo: make this async
            var result = from d in _doctorPatientDiseaseRepository.GetAll()
                         join pd in _personsRepository.GetAll()
                         on d.PersonId equals pd.Id
                         where doctorIds.Contains(d.DoctorId.Value)
                         select new
                         {
                             DoctorId = d.DoctorId,
                             Pacient = d.Person
                         };

            return result.ToLookup(item => item.DoctorId.Value, item => item.Pacient);
        }

        public async Task<ILookup<int, Disease>> GetDiseasesForDoctorLookUp(IEnumerable<int> doctorIds)
        {
            // todo: make this async
            var result = from d in _doctorPatientDiseaseRepository.GetAll()
                         join disease in _diseasesRepository.GetAll()
                         on d.DiseaseId equals disease.Id
                         where doctorIds.Contains(d.DoctorId.Value)
                         select new
                         {
                             DoctorId = d.DoctorId,
                             Disease = d.Disease
                         };

            return result.ToLookup(item => item.DoctorId.Value, item => item.Disease);
        }

        public async Task<ILookup<int, Doctor>> GetDoctorForPatientLookUp(IEnumerable<int> personIds)
        {
            // todo: make this async
            var result = from d in _doctorPatientDiseaseRepository.GetAll()
                         join doctor in _doctorRepository.GetAll()
                         on d.DoctorId equals doctor.Id
                         where personIds.Contains(d.PersonId)
                         select new
                         {
                             PersonId = d.PersonId,
                             Doctor = d.Doctor
                         };

            return result.ToLookup(item => item.PersonId, item => item.Doctor);
        }
    }
}
