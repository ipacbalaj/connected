using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Connected.Database;
using Connected.Database.Repositories;
using Connected.Graphql;
using Connected.Graphql.Queries;
using Connected.Middlewares;
using Connected.Services;
using GraphQL;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Connected
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ConnectedDbContext>();
            services.AddScoped<PersonsRepository>();
            services.AddScoped<DiseasesRepository>();
            services.AddScoped<PersonDiseaseRepository>();
            services.AddScoped<DoctorRepository>();
            services.AddScoped<DoctorPatientDiseaseRepository>();
            services.AddScoped<DiseasesService>();

            services.AddScoped<IDependencyResolver>(x => new FuncDependencyResolver(x.GetRequiredService));
            services.AddScoped<ConnectedSchema>();

            // this is needed for sync reading from database
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });


            services.AddGraphQL(x =>
            {
                x.ExposeExceptions = true;
            }).AddGraphTypes(ServiceLifetime.Scoped).AddDataLoader();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ConnectedDbContext dbContext)
        {

            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseGraphQL<ConnectedSchema>();
            app.UseGraphQLPlayground(new GraphQLPlaygroundOptions());
            //app.UseRouting();
            //app.UseAuthorization();

            dbContext.Seed();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
        }
    }
}
